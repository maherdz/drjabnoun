package com.machinestalk.drjabnoun.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.models.ItemsViewModel
import com.machinestalk.drjabnoun.ui.activities.ActualityDetailsActivity
import com.machinestalk.drjabnoun.ui.activities.LoginActivity
import de.hdodenhof.circleimageview.CircleImageView

class CustomAdapter(val fragment: Fragment, private val mList: List<ItemsViewModel>) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    var currentView: View? = null

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        currentView = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)
        return ViewHolder(currentView!!)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = mList[position]

        // sets the image to the imageview from our itemHolder class
        holder.imageView.setBackgroundResource(ItemsViewModel.image)

        // sets the text to the textview from our itemHolder class
        holder.textView.text = ItemsViewModel.title
        currentView?.setOnClickListener {
            navigateToMainActivity()
        }
    }


    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: CircleImageView = itemView.findViewById(R.id.imageview)
        val textView: TextView = itemView.findViewById(R.id.textView)
    }

    fun navigateToMainActivity() {
        val intent = Intent(fragment.context, ActualityDetailsActivity::class.java)
        fragment.startActivity(intent)
    }

}
