package com.machinestalk.drjabnoun.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.models.ItemsViewModel
import com.machinestalk.drjabnoun.ui.activities.ActualityDetailsActivity
import com.machinestalk.drjabnoun.ui.activities.LoginActivity
import de.hdodenhof.circleimageview.CircleImageView

class CustomFilAttenteAdapter(val fragment: Fragment, private val mList: List<ItemsViewModel>) :
    RecyclerView.Adapter<CustomFilAttenteAdapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item_attente, parent, false)
        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = mList[position]

        // sets the image to the imageview from our itemHolder class
        holder.imageView.setBackgroundResource(ItemsViewModel.image)

        // sets the text to the textview from our itemHolder class
        holder.textView.text = ItemsViewModel.title
        holder.imageView.setOnClickListener({
//            navigateoMainActivity()
        })
    }
//    fun navigateoMainActivity() {
//        val intent = Intent(fragment.context, ActualityDetailsActivity::class.java)
//        fragment.startActivity(intent)
//    }


    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: CircleImageView = itemView.findViewById(R.id.imageview)
        val textView: TextView = itemView.findViewById(R.id.textView)
    }
}
