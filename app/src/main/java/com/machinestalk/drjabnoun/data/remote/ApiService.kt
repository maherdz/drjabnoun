package com.machinestalk.drjabnoun.data.remote

import com.machinestalk.drjabnoun.data.remote.body.LoginBody
import com.machinestalk.drjabnoun.data.remote.body.SignUpBody
import com.machinestalk.drjabnoun.data.responses.Article
import com.machinestalk.drjabnoun.data.responses.LoginResponse
import com.machinestalk.drjabnoun.data.responses.SignUpResponse
import com.machinestalk.drjabnoun.helper.Constants
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @POST(Constants.LOGIN_URL)
    suspend fun authenticate(@Body loginBody: LoginBody): Response<LoginResponse>

    @POST(Constants.SIN_UP_URL)
    suspend fun register(@Body signUpBody: SignUpBody): Response<SignUpResponse>

    @GET(Constants.GET_ARTICLES)
    suspend fun getArticlesList(@Header("Authorization") accessToken: String): Response<List<Article>>
}