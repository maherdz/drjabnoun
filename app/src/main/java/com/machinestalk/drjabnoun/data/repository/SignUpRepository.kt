package com.machinestalk.drjabnoun.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.machinestalk.drjabnoun.data.remote.UserRemoteDataSource
import com.machinestalk.drjabnoun.data.remote.body.SignUpBody
import com.machinestalk.drjabnoun.data.responses.SignUpResponse
import com.machinestalk.drjabnoun.utils.Resource
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class SignUpRepository @Inject constructor(private val userRemoteDataSource: UserRemoteDataSource) {

    fun registerUser(signUpBody: SignUpBody): LiveData<Resource<SignUpResponse>> =
        liveData {
            Dispatchers.IO
            emit(Resource.loading())
            val response = userRemoteDataSource.register(signUpBody)
            if (response.status == Resource.Status.SUCCESS){
                emit(Resource.success(response.data!!))
            }else if (response.status == Resource.Status.ERROR){
                emit(Resource.error(response.message!!))
            }
        }
}