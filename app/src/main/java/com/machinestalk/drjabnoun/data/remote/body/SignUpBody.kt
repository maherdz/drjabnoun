package com.machinestalk.drjabnoun.data.remote.body

data class SignUpBody(
    private val name: String,
    private val Address: String,
    private val phoneNumber: String,
    private val email: String
) {
}