package com.machinestalk.drjabnoun.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.machinestalk.drjabnoun.data.remote.UserRemoteDataSource
import com.machinestalk.drjabnoun.data.remote.body.LoginBody
import com.machinestalk.drjabnoun.data.responses.LoginResponse
import com.machinestalk.drjabnoun.utils.Resource
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class LoginRepository @Inject constructor(private val userRemoteDataSource: UserRemoteDataSource) {

    fun authenticate(loginBody: LoginBody): LiveData<Resource<LoginResponse>> =
        liveData {
            Dispatchers.IO
            emit(Resource.loading())
            val responseStatus = userRemoteDataSource.authenticate(loginBody)
            if (responseStatus.status == Resource.Status.SUCCESS) {
                //saveCallResult(responseStatus.data!!)
                emit(Resource.success(responseStatus.data!!))

            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }
        }

}