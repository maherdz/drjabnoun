package com.machinestalk.drjabnoun.data.entities

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Category(
    val deleted: Boolean,
    val id: Int,
    val label: String
) : Parcelable