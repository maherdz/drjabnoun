package com.machinestalk.drjabnoun.data.remote

import com.machinestalk.drjabnoun.data.remote.body.LoginBody
import com.machinestalk.drjabnoun.data.remote.body.SignUpBody
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(private val apiService: ApiService): BaseDataSource() {

    suspend fun authenticate(loginBody: LoginBody) = getResult { apiService.authenticate(loginBody) }
    suspend fun register(signUpBody: SignUpBody) = getResult { apiService.register(signUpBody) }
    suspend fun getArticlesList(accessToken: String) = getResult { apiService.getArticlesList(accessToken) }
}