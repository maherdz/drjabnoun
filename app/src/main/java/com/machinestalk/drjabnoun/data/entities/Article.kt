package com.machinestalk.drjabnoun.data.responses

import android.os.Parcelable
import com.machinestalk.drjabnoun.data.entities.Category
import kotlinx.parcelize.Parcelize

@Parcelize
data class Article(
    val category: Category,
    val deleted: Boolean,
    val fileName: String,
    val id: Int,
    val ratings: Int,
    val text: String,
    val title: String
) : Parcelable