package com.machinestalk.drjabnoun.data.responses

data class LoginResponse (val id_token : String)