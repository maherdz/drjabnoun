package com.machinestalk.drjabnoun.data.responses

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.machinestalk.drjabnoun.data.remote.UserRemoteDataSource
import com.machinestalk.drjabnoun.utils.Resource
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class ArticleRepository @Inject constructor(private val userRemoteDataSource: UserRemoteDataSource) {

    fun getArticless(accessToken: String): LiveData<Resource<List<Article>>> =
        liveData {
            Dispatchers.IO
            emit(Resource.loading())
            val response = userRemoteDataSource.getArticlesList(accessToken)
            if (response.status == Resource.Status.SUCCESS){
                emit(Resource.success(response.data!!))
            }else if  (response.status == Resource.Status.ERROR){
                emit(Resource.error(response.message!!))
            }
        }

    suspend fun getArticles(accessToken: String): Resource<List<Article>> {
        return userRemoteDataSource.getArticlesList(accessToken)
    }
}