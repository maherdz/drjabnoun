package com.machinestalk.drjabnoun.models

data class ItemsViewModel(
    val image: Int,
    val title: String,
    val subTitle: String,
    val desc: String
) {
}
