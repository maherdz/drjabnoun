package com.machinestalk.drjabnoun.ui.fragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.adapter.CustomAdapter
import com.machinestalk.drjabnoun.adapter.CustomFilAttenteAdapter
import com.machinestalk.drjabnoun.models.ItemsViewModel

class FilAttenteFagment : Fragment() {

    private lateinit var rootView: View
    private lateinit var recyclerview: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_file_attente, container, false)
        // getting the recyclerview by its id
        val recyclerview = rootView.findViewById<RecyclerView>(R.id.recyclerview)

        // this creates a vertical layout Manager
        recyclerview.layoutManager = LinearLayoutManager(context)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        for (i in 1..7) {
            when (i) {
                1 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )
                }
                2 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )

                }
                3 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )
                }
                4 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )

                }
                5 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )

                }
                6 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )

                }
                7 -> {
                    data.add(
                        ItemsViewModel(
                            R.drawable.bg_img,
                            "ALLERGOLOGIE - IMMUNOLOGIE", "Allergie aux piqûres de moustiques",
                            "Bien que des maladies graves puissent être transmises par les moustiques, les manifestations les plus fréquentes sont ... "
                        )
                    )

                }
            }

        }

        // This will pass the ArrayList to our Adapter
        val adapter = CustomFilAttenteAdapter(this,data)


        // Setting the Adapter with the recyclerview
        recyclerview.adapter = adapter

        return rootView
    }
}