package com.machinestalk.drjabnoun.ui.fragment

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.databinding.FragmentProfileBinding
import com.machinestalk.drjabnoun.helper.IPreferenceHelper
import com.machinestalk.drjabnoun.helper.PrefsHelper
import com.machinestalk.drjabnoun.ui.activities.LoginActivity
import com.machinestalk.drjabnoun.ui.activities.MainActivity

class ProfilFagment : Fragment() {
    private lateinit var rootView: View
    private lateinit var view_profile: LinearLayout
    private lateinit var view_profile_detail: RelativeLayout
    private lateinit var profile_label: TextView
    private lateinit var binding: FragmentProfileBinding
    private val preferenceHelper: IPreferenceHelper by lazy { PrefsHelper(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater,container,false)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.profileLabel.setOnClickListener {
            view_profile_detail.visibility = View.VISIBLE
            view_profile.visibility = View.GONE
            Toast.makeText(context, "test", Toast.LENGTH_SHORT).show()

        }

        binding.logoutButton.setOnClickListener {
            val intent = Intent(requireActivity(), LoginActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
            preferenceHelper.setLoggedState(false)
        }

    }

}