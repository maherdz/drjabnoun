package com.machinestalk.drjabnoun.ui.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.machinestalk.drjabnoun.data.remote.body.LoginBody
import com.machinestalk.drjabnoun.data.repository.LoginRepository
import com.machinestalk.drjabnoun.data.responses.LoginResponse
import com.machinestalk.drjabnoun.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel
@Inject
constructor(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginBody = MutableLiveData<LoginBody>()

    private val _loginResponse = _loginBody.switchMap { loginBody ->
        loginRepository.authenticate(loginBody)
    }

    val loginResponse: LiveData<Resource<LoginResponse>> = _loginResponse

    fun start(loginBody: LoginBody){
        _loginBody.value = loginBody
    }
}