package com.machinestalk.drjabnoun.ui.fragment

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.ModifierInfo
import androidx.compose.ui.layout.VerticalAlignmentLine
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.adapter.CustomAdapter
import com.machinestalk.drjabnoun.data.responses.Article
import com.machinestalk.drjabnoun.helper.IPreferenceHelper
import com.machinestalk.drjabnoun.helper.PrefsHelper
import com.machinestalk.drjabnoun.models.ItemsViewModel
import com.machinestalk.drjabnoun.ui.activities.ActualityDetailsActivity
import com.machinestalk.drjabnoun.ui.viewModels.AcceuilViewModel
import com.machinestalk.drjabnoun.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AcueilFagment : Fragment() {
    private val viewModel: AcceuilViewModel by viewModels()
    private val preferenceHelper: IPreferenceHelper by lazy { PrefsHelper(requireContext()) }

    private val localFont = FontFamily(
        Font(R.font.opensans_bold, FontWeight.Bold),
        Font(R.font.opensans_regular, FontWeight.Normal),
        Font(R.font.opensans_light, FontWeight.Light),
        Font(R.font.opensans_semibold, FontWeight.SemiBold),
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val accessToken = preferenceHelper.getAccessToken()
        viewModel.start("Bearer " + accessToken)

        return ComposeView(requireContext()).apply {
            setContent {
                val articles = viewModel.articles?.value
                MaterialTheme {
                    background()
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        titleText()
                        searchLayout()
                        Spacer(modifier = Modifier.height(30.dp))
                        Conversation(articles!!)
                    }
                }
            }
        }
    }

    @Composable
    fun background() {
        Image(
            painter = painterResource(id = R.drawable.ic_bg_top_accueil),
            contentDescription = null,
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.Crop
        )
    }

    @Composable
    fun titleText() {
        Text(
            text = "Bonjour, Mr Ahmed",
            color = Color.White,
            modifier = Modifier.padding(0.dp, 30.dp, 0.dp, 0.dp),
            fontFamily = localFont,
            fontWeight = FontWeight.SemiBold
        )
    }

    @Composable
    fun searchLayout() {
        val inputValue = remember { mutableStateOf(TextFieldValue()) }

        Surface(
            modifier = Modifier
                .padding(30.dp, 30.dp, 30.dp, 0.dp)
                .clip(CircleShape)
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.White)
                    .fillMaxWidth()

            )
            {

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = Color.White),
                    verticalAlignment = Alignment.CenterVertically
                )
                {
                    Image(
                        painter = painterResource(id = R.drawable.ic_search_icon),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(15.dp, 0.dp, 0.dp, 0.dp)
                            .size(20.dp)

                    )

                    TextField(
                        colors = TextFieldDefaults.textFieldColors(
                            textColor = Color.Black,
                            disabledTextColor = Color.Transparent,
                            backgroundColor = Color.White,
                            focusedIndicatorColor = Color.Transparent,
                            unfocusedIndicatorColor = Color.Transparent,
                            disabledIndicatorColor = Color.Transparent
                        ),
                        value = inputValue.value,
                        onValueChange = {
                            inputValue.value = it
                            viewModel.searchByArticle(inputValue.value.text)
                        },
                        placeholder = {
                            Text(text = "Chercher")
                        }
                    )
                }
            }
        }


    }

    @Composable
    fun Conversation(articles: List<Article>) {
        LazyColumn {
            items(articles) { item ->
                ItemCard(item)
            }
        }
    }

    @Composable
    fun ItemCard(article: Article) {
        Surface(
            shape = MaterialTheme.shapes.medium,
            elevation = 6.dp,
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth()
                .clickable {
                    val intent = Intent(
                        requireContext(),
                        ActualityDetailsActivity::class.java
                    )
                    intent.putExtra("ArticleDetails", article)
                    requireActivity().startActivity(intent)
                }
        )

        {

            Row(
                modifier = Modifier
                    .padding(all = 8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = painterResource(R.drawable.bg_img),
                    contentDescription = null,
                    modifier = Modifier
                        .size(80.dp)
                        .clip(CircleShape)
                    //.border(1.5.dp, MaterialTheme.colors.secondaryVariant, CircleShape)
                )
                Spacer(modifier = Modifier.width(8.dp))

                // We toggle the isExpanded variable when we click on this Column
                Column()

                {
                    Text(
                        text = article.category.label,
                        color = Color.Gray,
                        fontSize = 14.sp,
                        fontFamily = localFont,
                        fontWeight = FontWeight.Normal
                    )

                    Spacer(modifier = Modifier.height(2.dp))

                    Text(
                        text = article.title,
                        modifier = Modifier.padding(all = 4.dp),
                        fontSize = 14.sp,
                        fontFamily = localFont,
                        fontWeight = FontWeight.Bold
                    )

                    Spacer(modifier = Modifier.height(2.dp))

                    Text(
                        text = article.text,
                        modifier = Modifier.padding(all = 4.dp),
                        fontSize = 10.sp,
                        fontFamily = localFont,
                        fontWeight = FontWeight.Normal,
                        maxLines = 2,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
    }


    @Preview
    @Composable
    fun PreviewData() {

        background()
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            titleText()
            searchLayout()
            Spacer(modifier = Modifier.height(30.dp))
            //Conversation(itemsViewModels = prepareDataList())
        }
    }
}