package com.machinestalk.drjabnoun.ui.fragment

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.machinestalk.drjabnoun.R

class RendezVousFagment : Fragment() {
    private lateinit var rootView: View
    private lateinit var text1: TextView
    private lateinit var text2: TextView
    private lateinit var text3: TextView
    private lateinit var text4: TextView
    private lateinit var linearlayout1: LinearLayout
    private lateinit var linearlayout2: LinearLayout
    private lateinit var linearlayout3: LinearLayout
    private lateinit var linearlayout4: LinearLayout

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_rendez_vous, container, false)
        // getting the recyclerview by its id
         text1 = rootView.findViewById<TextView>(R.id.text1)
         text2 = rootView.findViewById<TextView>(R.id.text2)
         text3 = rootView.findViewById<TextView>(R.id.text3)
         text4 = rootView.findViewById<TextView>(R.id.text4)
         linearlayout1 = rootView.findViewById<LinearLayout>(R.id.linearlayout1)
         linearlayout2 = rootView.findViewById<LinearLayout>(R.id.linearlayout2)
         linearlayout3 = rootView.findViewById<LinearLayout>(R.id.linearlayout3)
         linearlayout4 = rootView.findViewById<LinearLayout>(R.id.linearlayout4)

        linearlayout1.setOnClickListener {
            linearlayout1.setBackgroundResource(R.color.bootom_nav)
            text1.setTextColor(requireActivity().getColor(R.color.white))
            text2.setTextColor(requireActivity().getColor(R.color.black))
            text3.setTextColor(requireActivity().getColor(R.color.black))
            text4.setTextColor(requireActivity().getColor(R.color.black))
            linearlayout2.setBackgroundResource(R.color.white)
            linearlayout3.setBackgroundResource(R.color.white)
            linearlayout4.setBackgroundResource(R.color.white)

        }
        linearlayout2.setOnClickListener {
            linearlayout2.setBackgroundResource(R.color.bootom_nav)
            text2.setTextColor(requireActivity().getColor(R.color.white))
            text1.setTextColor(requireActivity().getColor(R.color.black))
            text3.setTextColor(requireActivity().getColor(R.color.black))
            text4.setTextColor(requireActivity().getColor(R.color.black))

            linearlayout1.setBackgroundResource(R.color.white)
            linearlayout3.setBackgroundResource(R.color.white)
            linearlayout4.setBackgroundResource(R.color.white)

        }
        linearlayout3.setOnClickListener {
            linearlayout3.setBackgroundResource(R.color.bootom_nav)
            text3.setTextColor(requireActivity().getColor(R.color.white))
            text2.setTextColor(requireActivity().getColor(R.color.black))
            text1.setTextColor(requireActivity().getColor(R.color.black))
            text4.setTextColor(requireActivity().getColor(R.color.black))

            linearlayout2.setBackgroundResource(R.color.white)
            linearlayout1.setBackgroundResource(R.color.white)
            linearlayout4.setBackgroundResource(R.color.white)

        }
        linearlayout4.setOnClickListener {
            linearlayout4.setBackgroundResource(R.color.bootom_nav)
            text4.setTextColor(requireActivity().getColor(R.color.white))
            text2.setTextColor(requireActivity().getColor(R.color.black))
            text3.setTextColor(requireActivity().getColor(R.color.black))
            text1.setTextColor(requireActivity().getColor(R.color.black))

            linearlayout2.setBackgroundResource(R.color.white)
            linearlayout3.setBackgroundResource(R.color.white)
            linearlayout1.setBackgroundResource(R.color.white)

        }
        return rootView
    }
}