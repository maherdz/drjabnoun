package com.machinestalk.drjabnoun.ui.viewModels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.machinestalk.drjabnoun.data.remote.body.LoginBody
import com.machinestalk.drjabnoun.data.responses.Article
import com.machinestalk.drjabnoun.data.responses.ArticleRepository
import com.machinestalk.drjabnoun.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AcceuilViewModel
@Inject constructor(private val articleRepository: ArticleRepository) :
    ViewModel() {

    private val _articles: MutableState<List<Article>> = mutableStateOf(listOf())
    var articles: MutableState<List<Article>>? = mutableStateOf(listOf())

    fun start(accessToken: String) {
        viewModelScope.launch {

            val response = articleRepository.getArticles(accessToken)
            if (response.status == Resource.Status.SUCCESS) {
                articles?.value = response.data!!
                _articles.value = response.data
            } else if (response.status == Resource.Status.ERROR) {

            }
        }
    }

    fun searchByArticle(query: String) {
        articles?.value = _articles.value

        articles?.value = articles?.value!!.filter {
            it.title.contains(query, true) || it.category.label.contains(query, true)
        }
    }
}