package com.machinestalk.drjabnoun.ui.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.machinestalk.drjabnoun.data.remote.body.SignUpBody
import com.machinestalk.drjabnoun.data.repository.SignUpRepository
import com.machinestalk.drjabnoun.data.responses.SignUpResponse
import com.machinestalk.drjabnoun.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel
@Inject
constructor(private val signUpRepository: SignUpRepository): ViewModel() {

    private val _signUpBody = MutableLiveData<SignUpBody>()

    private val _signUpResponse = _signUpBody.switchMap { signUpBody ->
        signUpRepository.registerUser(signUpBody)
    }

    val signUpResponse: LiveData<Resource<SignUpResponse>> = _signUpResponse

    fun start(signUpBody: SignUpBody){
        _signUpBody.value = signUpBody
    }
}