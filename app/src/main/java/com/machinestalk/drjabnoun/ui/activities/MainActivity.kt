package com.machinestalk.drjabnoun.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.ui.fragment.AcueilFagment
import com.machinestalk.drjabnoun.ui.fragment.FilAttenteFagment
import com.machinestalk.drjabnoun.ui.fragment.ProfilFagment
import com.machinestalk.drjabnoun.ui.fragment.RendezVousFagment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    var bottomNavigationView: BottomNavigationView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView = findViewById(R.id.bottomNavigationView)

        val acueilFagment= AcueilFagment()
        val rendezVousFagment= RendezVousFagment()
        val filAttenteFagment= FilAttenteFagment()
        val profilFagment= ProfilFagment()

        setCurrentFragment(acueilFagment)

        bottomNavigationView!!.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.accueil->setCurrentFragment(acueilFagment)
                R.id.rendez_vous->setCurrentFragment(rendezVousFagment)
                R.id.fAttente->setCurrentFragment(filAttenteFagment)
                R.id.profile->setCurrentFragment(profilFagment)
            }
            true
        }





//        signupBtn = findViewById(R.id.createAccount_btn)
//        signupBtn!!.setOnClickListener({
//            startActivity(Intent(this,SignUpScreen::class.java))
//        })
    }

    private fun setCurrentFragment(fragment: Fragment)=
        supportFragmentManager.beginTransaction().apply { replace(R.id.flFragment,fragment)
            commit()
        }
}