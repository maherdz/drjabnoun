package com.machinestalk.drjabnoun.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.machinestalk.drjabnoun.data.remote.body.LoginBody
import com.machinestalk.drjabnoun.data.responses.LoginResponse
import com.machinestalk.drjabnoun.databinding.ActivityLoginBinding
import com.machinestalk.drjabnoun.helper.IPreferenceHelper
import com.machinestalk.drjabnoun.helper.PrefsHelper
import com.machinestalk.drjabnoun.ui.viewModels.LoginViewModel
import com.machinestalk.drjabnoun.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private var loginBinding: ActivityLoginBinding? = null
    private val viewModel: LoginViewModel by viewModels()

    private val preferenceHelper: IPreferenceHelper by lazy { PrefsHelper(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginBinding = ActivityLoginBinding.inflate(layoutInflater)
        val view = loginBinding?.root
        setContentView(view)

        loginBinding?.signupText?.setOnClickListener {
            startActivity(Intent(this, SignUpScreen::class.java))
        }

        loginBinding?.signInButton?.setOnClickListener {
            if (verifyData()) {
                val loginBody = LoginBody(
                    loginBinding?.viewLoginMailEditText?.text.toString(),
                    loginBinding?.viewLoginPasswordEditText?.text.toString()
                )

                viewModel.start(loginBody)
                setupObservers()
            }else{
                Toast.makeText(
                    this,
                    "Champ vide, veuillez saisir vos données SVP",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun verifyData(): Boolean {
        return !loginBinding?.viewLoginMailEditText?.text.isNullOrEmpty()
                && !loginBinding?.viewLoginPasswordEditText?.text.isNullOrEmpty()
    }

    private fun setupObservers(){
        viewModel.loginResponse.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    val response : LoginResponse? = it.data
                    val token: String? = response?.id_token
                    navigateToMainActivity(token!!)
                    loginBinding?.progressBar?.visibility = View.GONE
                }

                Resource.Status.ERROR ->{
                    loginBinding?.progressBar?.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }

                Resource.Status.LOADING -> {
                    loginBinding?.progressBar?.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun navigateToMainActivity(token: String) {
        preferenceHelper.setAccessToken(token)
        preferenceHelper.setLoggedState(true)
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()

    }
}