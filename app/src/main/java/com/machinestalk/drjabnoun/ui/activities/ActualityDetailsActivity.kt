package com.machinestalk.drjabnoun.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.machinestalk.drjabnoun.data.responses.Article
import com.machinestalk.drjabnoun.databinding.ActivityActualityDetailBinding

class ActualityDetailsActivity : AppCompatActivity() {
    var long_desc: TextView? = null
    private var binding: ActivityActualityDetailBinding? = null
    private var article: Article? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityActualityDetailBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        article = intent.getParcelableExtra("ArticleDetails")

        binding?.title?.text = article?.title
        binding?.description?.text = article?.text
        binding?.label?.text = article?.category?.label
    }

}