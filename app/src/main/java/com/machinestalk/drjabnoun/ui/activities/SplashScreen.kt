package com.machinestalk.drjabnoun.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.helper.IPreferenceHelper
import com.machinestalk.drjabnoun.helper.PrefsHelper

class SplashScreen : AppCompatActivity() {
    private val preferenceHelper: IPreferenceHelper by lazy { PrefsHelper(applicationContext) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val isLogged = preferenceHelper.isLogged()

        Handler(Looper.getMainLooper()).postDelayed({
            val intent: Intent = if (isLogged) {
                Intent(this, MainActivity::class.java)
            } else {
                Intent(this, LoginActivity::class.java)
            }
            startActivity(intent)
            finish()
        }, 1000)

    }

}