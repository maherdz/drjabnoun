package com.machinestalk.drjabnoun.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.machinestalk.drjabnoun.R
import com.machinestalk.drjabnoun.data.remote.body.SignUpBody
import com.machinestalk.drjabnoun.databinding.ActivityLoginBinding
import com.machinestalk.drjabnoun.databinding.ActivitySignupBinding
import com.machinestalk.drjabnoun.ui.viewModels.SignUpViewModel
import com.machinestalk.drjabnoun.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpScreen : AppCompatActivity() {
    private var binding: ActivitySignupBinding? = null
    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignupBinding.inflate(layoutInflater)
        val view = binding?.root
        setContentView(view)

        binding?.identifyButton?.setOnClickListener {
            if (verifyData()) {
                val signUpBody = SignUpBody(
                    binding?.viewNameEditText?.text.toString(),
                    binding?.viewAddressEditText?.text.toString(),
                    binding?.viewPhoneNumberEditText?.text.toString(),
                    binding?.viewEmailEditText?.text.toString(),
                )
                viewModel.start(signUpBody)
                setupObservers()
            } else {
                Toast.makeText(
                    this,
                    "Champ vide, veuillez saisir vos données SVP",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun verifyData(): Boolean {
        return !binding?.viewAddressEditText?.text.isNullOrEmpty()
                && !binding?.viewEmailEditText?.text.isNullOrEmpty()
                && !binding?.viewNameEditText?.text.isNullOrEmpty()
                && !binding?.viewPhoneNumberEditText?.text.isNullOrEmpty()
    }

    private fun setupObservers() {
        viewModel.signUpResponse.observe(this, Observer {
            when (it.status) {
                Resource.Status.LOADING -> {
                    binding?.progressBar?.visibility = View.VISIBLE
                }

                Resource.Status.SUCCESS -> {
                    binding?.progressBar?.visibility = View.GONE
                    finish()
                }

                Resource.Status.ERROR -> {
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                    binding?.progressBar?.visibility = View.GONE
                }
            }
        })
    }
}