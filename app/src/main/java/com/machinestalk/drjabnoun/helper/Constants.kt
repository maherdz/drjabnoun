package com.machinestalk.drjabnoun.helper

object Constants {

    const val BASE_URL = "http://178.238.225.159:8080/"

    //api end points
    const val LOGIN_URL = "api/authenticate"
    const val SIN_UP_URL = "api/register"
    const val GET_ARTICLES = "api/articles"
}