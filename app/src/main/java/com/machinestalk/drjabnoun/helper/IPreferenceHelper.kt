package com.machinestalk.drjabnoun.helper

interface IPreferenceHelper {
    fun setAccessToken(accessToken: String)
    fun getAccessToken(): String
    fun setLoggedState(isLogged: Boolean)
    fun isLogged(): Boolean
    fun clearPrefs()
}