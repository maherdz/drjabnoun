package com.machinestalk.drjabnoun.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.machinestalk.drjabnoun.data.local.AppDatabase
import com.machinestalk.drjabnoun.data.remote.ApiService
import com.machinestalk.drjabnoun.data.remote.UserRemoteDataSource
import com.machinestalk.drjabnoun.data.repository.LoginRepository
import com.machinestalk.drjabnoun.helper.Constants
import com.machinestalk.drjabnoun.helper.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Module {

    @Provides
    fun provideBaseUrl() = BASE_URL

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(apiService: ApiService) = UserRemoteDataSource(apiService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext applicationContext: Context) = AppDatabase.getDatabase(applicationContext)

    @Singleton
    @Provides
    fun provideUserDao(db: AppDatabase) = db.userDao()

    @Singleton
    @Provides
    fun provideRepository(userRemoteDataSource: UserRemoteDataSource) = LoginRepository(userRemoteDataSource)


}